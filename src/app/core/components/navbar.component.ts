import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'mg-navbar',
  template: `
    <button [routerLink]="'login'">login</button>
    <button routerLink="uikit">uikit</button>
    <button routerLink="catalog">Catalog</button>
    <button routerLink="contacts">Contacts</button>
    <button routerLink="admin">admin</button>
    <hr>
  `,
  styles: [
  ]
})
export class NavbarComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
