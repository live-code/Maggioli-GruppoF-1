import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from '../../model/user';
import { Post } from '../../model/post';

@Component({
  selector: 'mg-uikit-demo',
  template: `
    <div class="container">
      <mg-static-map city="Trieste"></mg-static-map>
      <!--Stateless Card -->
      <button (click)="visible = true">Open</button>
      <mg-card
        [opened]="visible"
        (toggle)="visible = !visible"
        title="ciao">miao</mg-card>

      <mg-card
        *ngIf="visible"
        title="Maggioli"
        icon="fa fa-link"
        [opened]="visible2"
        (toggle)="visible2 = !visible2"
      >
        <h1>ciao</h1>
        <h2>miao</h2>
      </mg-card>

      <!--Post list-->
      <mg-list [items]="posts" labelField="title"
               icon="fa fa-link"
               (doSome)="doSomething($event)"
               (iconClick)="openLink($event)"></mg-list>


      <!--List (left) + details (right)-->
      <div class="row">
        <div class="col">
          <mg-list
            [items]="users"
            [active]="activeUser"
            icon="fa fa-trash"
            (itemClick)="showDescription($event)"
            (iconClick)="deleteUser($event)"
          ></mg-list>
        </div>

        <div class="col" *ngIf="activeUser">
          {{activeUser.name}}<hr>{{activeUser.email}}
        </div>
      </div>

    </div>

  `,
})
export class UikitDemoComponent implements OnInit {
  users: User[];
  posts: Post[];
  activeUser: User;
  visible = false;
  visible2 = false;

  constructor(private http: HttpClient) { }

  ngOnInit(): void {
    this.http.get<User[]>('https://jsonplaceholder.typicode.com/users')
      .subscribe(res => {
        this.users = res;
        this.activeUser = this.users[3];
      });

    this.http.get<Post[]>('https://jsonplaceholder.typicode.com/posts')
      .subscribe(res => {
        this.posts = res;
      });
  }

  showDescription(user: User): void {
    this.activeUser = user;
  }

  openLink(post: Post): void {
    console.log('post', post)
  }

  deleteUser(user: User): void {
    console.log('user', user)
  }

  doSomething(val: number): void {
    console.log(val)
  }
}
