import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UikitDemoComponent } from './uikit-demo.component';
import { SharedModule } from '../../shared/shared.module';
import { RouterModule } from '@angular/router';



@NgModule({
  declarations: [
    UikitDemoComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild([
      { path: '', component: UikitDemoComponent }
    ])
  ]
})
export class UikitDemoModule { }
