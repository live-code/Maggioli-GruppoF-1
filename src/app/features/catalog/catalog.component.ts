import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'mg-catalog',
  template: `
    <input type="text" ngModel>
    <mg-catalog-search></mg-catalog-search>
    <mg-catalog-list></mg-catalog-list>
    <mg-catalog-offers></mg-catalog-offers>
    
    <mg-card>bla bla</mg-card>

  `,
  styles: [
  ]
})
export class CatalogComponent {

}
