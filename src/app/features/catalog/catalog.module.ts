import { NgModule } from '@angular/core';
import { CatalogComponent } from './catalog.component';
import { CatalogSearchComponent } from './components/catalog-search.component';
import { CatalogListComponent } from './components/catalog-list.component';
import { CatalogOffersComponent } from './components/catalog-offers.component';
import { SharedModule } from '../../shared/shared.module';
import { CommonModule } from '@angular/common';
import { CatalogRoutingModule } from './catalog-routing.module';

@NgModule({
  declarations: [
    CatalogComponent,
    CatalogSearchComponent,
    CatalogListComponent,
    CatalogOffersComponent,
  ],
  imports: [
    CommonModule,
    CatalogRoutingModule,
    SharedModule,
  ]
})
export class CatalogModule { }
