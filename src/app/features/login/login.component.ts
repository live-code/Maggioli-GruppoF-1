import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, ValidationErrors, Validators } from '@angular/forms';


@Component({
  selector: 'mg-login',
  template: `
    <form [formGroup]="form">
      <input 
        type="text" placeholder="username" formControlName="user"
        [style.border]="form.get('user').invalid ? '2px solid red' : null"
      >
      <input type="text" placeholder="password" formControlName="pass">
      
      <button [disabled]="form.invalid">SAVE</button>
    </form>
  `,
})
export class LoginComponent implements OnInit {
  // form: FormGroup = new FormGroup({
  //   user: new FormControl(),
  //   pass: new FormControl(),
  // })
  form: FormGroup;

  constructor(private fb: FormBuilder) {
    this.form = fb.group({
      user: [null, Validators.compose([
        Validators.required, Validators.minLength(3), alphaNumeric
      ])],
      pass: [null, Validators.required],
    });

  }

  ngOnInit(): void {
  }

}

function alphaNumeric(c: FormControl): ValidationErrors |  null {
  const ALPHA_NUMERIC_REGEX = /^([A-Za-z]|[0-9]|_)+$/;
  if (c.value && !c.value.match(ALPHA_NUMERIC_REGEX)) {
    return { alphaNumeric: true};
  }
  return null;
}
