import { AfterViewInit, Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { fromEvent, Observable, Subject, Subscription } from 'rxjs';
import { debounce, debounceTime, distinctUntilChanged, filter, map, switchMap, takeUntil } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { User } from '../../model/user';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'mg-admin',
  template: `
    <h4>Example FormControl + Destroy</h4>
    <input type="text" [formControl]="usernameInput" placeholder="Try username 'Bret', 'Antonette'">
  `,
  styles: [
  ]
})
export class AdminComponent implements AfterViewInit, OnDestroy {
  destroy$ = new Subject();
  usernameInput: FormControl = new FormControl();

  constructor(private http: HttpClient) {}

  ngAfterViewInit(): void {
    this.usernameInput.valueChanges
      .pipe(
        takeUntil(this.destroy$),
        filter(text => text.length > 1),
        debounceTime(1000),
        distinctUntilChanged(),
        switchMap(text => this.http.get<User[]>('https://jsonplaceholder.typicode.com/users?username=' + text)),
        filter(users => users.length > 0),
        map(users => users[0].email)
      )
      .subscribe(email => console.log(email))
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
