import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'mg-list-item',
  template: `
    <li
      class="list-group-item"
      [ngClass]="{'active': item.id === active?.id }"
      (click)="itemClick.emit(item)"
    >
      {{item[labelField]}}

      <i class="fa fa-trash" 
         (click)="iconClick.emit(item)"></i>
      <i class="fa fa-info-circle" (click)="toggle()"></i>
      
      <button (click)="doSomething.emit(123)">do something</button>

      <div *ngIf="opened">
        contenuto del body
      </div>
    </li>
  `,
})
export class ListItemComponent {
  @Input() item: any;
  @Input() labelField = 'name';
  @Input() active: any;
  @Output() itemClick: EventEmitter<any> = new EventEmitter<any>();
  @Output() iconClick: EventEmitter<any> = new EventEmitter<any>();
  @Output() doSomething: EventEmitter<number> = new EventEmitter<any>();

  opened = false;

  toggle(): void {
    this.opened = !this.opened;
  }

}
