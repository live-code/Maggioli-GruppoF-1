import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'mg-card',
  template: `
    <div class="card">
      <div class="card-header" [ngClass]="headerCls" (click)="toggle.emit()"> 
        {{title}}
        <i *ngIf="icon" class="pull-right" 
           [ngClass]="icon" (click)="iconClick.emit()"> </i>
      </div>
      <div class="card-body" *ngIf="opened">
        <ng-content></ng-content>
      </div>
    </div>
  `,
})
export class CardComponent {
  @Input() title = 'Maggioli';
  @Input() headerCls = 'bg-info';
  @Input() icon: string;
  @Input() opened = true;
  @Output() iconClick: EventEmitter<void> = new EventEmitter();
  @Output() toggle: EventEmitter<void> = new EventEmitter();

}
