import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'mg-list',
  template: `
    {{active?.name}}
    <ul class="list-group">
      <!--filter-->
      
      <mg-list-item
        *ngFor="let item of items"
        [item]="item"
        [labelField]="labelField"
        (iconClick)="iconClick.emit($event)"
        (itemClick)="itemClick.emit($event)"
        (doSomething)="doSome.emit($event)"
      ></mg-list-item>
      
      <!--paginator-->
    </ul>
  `,
})
export class ListComponent {
  @Input() items: any[];
  @Input() active: any;
  @Input() icon: string;
  @Input() labelField = 'name';
  @Output() itemClick: EventEmitter<any> = new EventEmitter<any>();
  @Output() iconClick: EventEmitter<any> = new EventEmitter<any>();
  @Output() doSome: EventEmitter<number> = new EventEmitter<number>();
}

