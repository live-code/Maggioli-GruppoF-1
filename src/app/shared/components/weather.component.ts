import { Component, Input, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Meteo } from '../../model/meteo';
import { delay } from 'rxjs/operators';

const API = 'http://api.openweathermap.org/data/2.5';
const token = 'eb03b1f5e5afb5f4a4edb40c1ef2f534';

@Component({
  selector: 'mg-static-map',
  template: `
      <h1>{{city}}</h1>
      
      <div *ngIf="data; else loader">
        {{data.main.temp}}°
      </div>
      
      <ng-template #loader >
        loading.....
      </ng-template>
  `,
})
export class WeatherComponent implements OnInit {
  @Input() city: string;
  data: Meteo;

  constructor(private http: HttpClient) {}

  ngOnInit(): void {
    this.http.get<Meteo>(`${API}/weather?q=${this.city}&units=metric&APPID=${token}`)
      .pipe(
        delay(1000)
      )
      .subscribe(res => this.data = res)
  }
}
