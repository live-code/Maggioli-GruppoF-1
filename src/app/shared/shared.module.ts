import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardComponent } from './components/card.component';
import { ListComponent } from './components/list.component';
import { ListItemComponent } from './components/list-item.component';
import { WeatherComponent } from './components/weather.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    CardComponent,
    ListComponent,
    ListItemComponent,
    WeatherComponent,
  ],
  exports: [
    CardComponent,
    ListComponent,
    WeatherComponent,
  ],
  imports: [
    CommonModule,
  ]
})
export class SharedModule { }
