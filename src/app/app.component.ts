import { Component } from '@angular/core';
import { NavigationEnd, Router, RouterEvent } from '@angular/router';
import { filter, map } from 'rxjs/operators';

@Component({
  selector: 'mg-root',
  template: `
    <mg-navbar> </mg-navbar>
    <router-outlet></router-outlet>
  `,
})
export class AppComponent {

  constructor(private router: Router) {
    router.events
      .pipe(
        filter((evt: RouterEvent) => evt instanceof NavigationEnd),
        map(evt => evt.url)
      )
      .subscribe(url => {
        console.log('google analytics', url)
      })
  }
}
